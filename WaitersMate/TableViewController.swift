//
//  SecondViewController.swift
//  WaitersMate
//
//  Created by Admin on 28/10/2014.
//  Copyright (c) 2014 Coventry University. All rights reserved.
//

import UIKit

class TableViewController : UITableViewController, UIGestureRecognizerDelegate {
    
    // constants representing the index of the tabs
    let TAB_TABLES = 0
    let TAB_DRINKS = 1
    let TAB_FOODS = 2
    let TAB_ORDER = 3
    
    // table
    @IBOutlet var tableTable: UITableView!
    
    // swipe gesture
    @IBOutlet var swipeLeft: UISwipeGestureRecognizer!
    
    @IBAction func swipeLeftGesture(sender: UISwipeGestureRecognizer) {
        /*UIView.animateWithDuration(1, animations: {
            self.tableTable.alpha = 0
            }, completion: {
                (value: Bool) in
                self.tabBarController?.selectedIndex++
        })*/
        
        // for some reason, xcode won't build when using an animation
        // displaying a nonsensical error message about missing parameters
        // so page transistions are boring
        self.tabBarController?.selectedIndex++
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // set badge according to selected table
        self.tabBarItem.badgeValue = (model.tables[model.currentTableIndex]).description
        // ensure badge will always display the right value
        tableChangedEvent.addOnTableChangedClosure({
            self.tabBarItem.badgeValue = model.tables[model.currentTableIndex].description
        })
        // ensure table cells display correct info (about drinks/food NOT? being ordered
        orderChangedEvent.addOnOrderChangedClosure({
            self.tableTable.reloadData()
        })
        
        // set and update badge of order tab
        // have to do this here since order tab might be opened only (much) later
        // and order badge must be updated despite the ordet tab not being loaded
        let orderTabItem = ((self.tabBarController!.tabBar.items as [UITabBarItem]))[self.TAB_ORDER]
        orderTabItem.badgeValue = self.getCostOfOrderString()
        orderChangedEvent.addOnOrderChangedClosure({
            orderTabItem.badgeValue = self.getCostOfOrderString()
        })
        tableChangedEvent.addOnTableChangedClosure({
            orderTabItem.badgeValue = self.getCostOfOrderString()
        })
        
        self.view.addGestureRecognizer(self.swipeLeft)
        self.view.backgroundColor = tableColor
    }
    
    func getCostOfOrderString() -> String? {
        return model.getCostOfOrderForCurrentTable() == 0 ? nil : "£" + model.getCostOfOrderForCurrentTable().description
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the± section.
        
        return model.tables.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("TableItem", forIndexPath: indexPath) as UITableViewCell
        
        // Configure the cell...
        cell.textLabel.text = "Select table " + model.tables[indexPath.row].description
        cell.detailTextLabel?.text = "Have " + (model.hasTableOrderedDrinks(indexPath.row) ? "" : "NOT ") + "ordered drinks, have " + (model.hasTableOrderedFoods(indexPath.row) ? "" : "NOT ") + "ordered foods."
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.setCellColor(tableView.cellForRowAtIndexPath(indexPath)!, color: tableColorHighlighted)
        
        model.currentTableIndex = indexPath.row
        tableChangedEvent.onTableChanged()
        
        if (!model.hasCurrentTableOrderedDrinks()) {
            self.tabBarController?.selectedIndex = TAB_DRINKS
        }else if (!model.hasCurrentTableOrderedFoods()){
            self.tabBarController?.selectedIndex = TAB_FOODS
        }else{
            var alert:UIAlertController
            alert = UIAlertController(title: "Open tab", message: "Which tab do you want to open?", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Order drinks", style: UIAlertActionStyle.Default, handler: openDrinks))
            alert.addAction(UIAlertAction(title: "Order foods", style: UIAlertActionStyle.Default, handler: openFoods))
            alert.addAction(UIAlertAction(title: "Show order", style: UIAlertActionStyle.Default, handler: openOrder))
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    override func tableView(tableView: UITableView, didHighlightRowAtIndexPath indexPath: NSIndexPath) {
        self.setCellColor(tableView.cellForRowAtIndexPath(indexPath)!, color: tableColorHighlighted)
    }
    
    override func tableView(tableView: UITableView, didUnhighlightRowAtIndexPath indexPath: NSIndexPath) {
        self.setCellColor(tableView.cellForRowAtIndexPath(indexPath)!, color: tableColor)
    }
    
    func setCellColor(cell: UITableViewCell, color: UIColor){
        cell.contentView.backgroundColor = color
        cell.backgroundColor = color
    }
    
    func openDrinks(alert: UIAlertAction!){
        self.tabBarController?.selectedIndex = self.TAB_DRINKS
    }
    func openFoods(alert: UIAlertAction!){
        self.tabBarController?.selectedIndex = self.TAB_FOODS
    }
    func openOrder(alert: UIAlertAction!){
        self.tabBarController?.selectedIndex = self.TAB_ORDER
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView!, canEditRowAtIndexPath indexPath: NSIndexPath!) -> Bool {
    // Return NO if you do not want the specified item to be editable.
    return true
    }
    */
    
    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView!, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath!) {
    if editingStyle == .Delete {
    // Delete the row from the data source
    tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
    } else if editingStyle == .Insert {
    // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
    }
    */
    
    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView!, moveRowAtIndexPath fromIndexPath: NSIndexPath!, toIndexPath: NSIndexPath!) {
    
    }
    */
    
    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView!, canMoveRowAtIndexPath indexPath: NSIndexPath!) -> Bool {
    // Return NO if you do not want the item to be re-orderable.
    return true
    }
    */
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    }
    */
}

