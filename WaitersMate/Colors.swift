//
//  Colors.swift
//  WaitersMate
//
//  Created by admin on 12/6/14.
//  Copyright (c) 2014 Coventry University. All rights reserved.
//

import UIKit

let tableColor              = UIColor(red: 1, green: 0.8, blue: 0.3, alpha: 1)
let tableColorHighlighted   = UIColor(red: 0.9, green: 0.7, blue: 0.2, alpha: 1)
let drinksColor             = UIColor(red: 0.4, green: 0.7, blue: 1, alpha: 1)
let drinksColorHighlighted  = UIColor(red: 0.3, green: 0.6, blue: 0.9, alpha: 1)
let foodsColor              = UIColor(red: 0.4, green: 1, blue: 0.5, alpha: 1)
let foodsColorHighlighted   = UIColor(red: 0.3, green: 0.9, blue: 0.4, alpha: 1)
let orderColor              = UIColor(red: 1, green: 1, blue: 0.3, alpha: 1)
let orderColorHighlighted   = UIColor(red: 0.9, green: 0.9, blue: 0.2, alpha: 1)