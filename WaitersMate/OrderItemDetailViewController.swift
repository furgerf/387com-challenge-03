

import UIKit

class OrderItemDetailViewController : UIViewController
{
    // variables for passed data
    var orderItem:OrderItem?
    var amountOrdered:Int?
    var isDrink:Bool?
    var completionHandler:(() -> Void)?
    var color:UIColor?
    
    // outlets
    @IBOutlet weak var navigationBar: UINavigationBar!
    @IBOutlet weak var labelOrdered: UILabel!
    @IBOutlet weak var priceOfMany: UILabel!
    @IBOutlet weak var orderMany: UIButton!
    @IBOutlet weak var sliderOrder: UISlider!
    @IBOutlet weak var removeMany: UIButton!
    @IBOutlet weak var sliderRemove: UISlider!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        if let item:OrderItem = orderItem {
            self.orderMany.setTitle("Order \(Int(self.sliderOrder.value)) \(item.Description)", forState: UIControlState.Normal)
            self.removeMany.setTitle("Remove \(Int(self.sliderOrder.value)) \(item.Description)", forState: UIControlState.Normal)
            
            self.navigationBar.topItem?.title = item.Description
            self.priceOfMany.text = "Price of \(Int(self.sliderOrder.value)) \(item.Description): £\(item.Price * self.sliderOrder.value)"

            if let drink:Bool = isDrink {
                self.sliderRemove.maximumValue = Float(drink ? model.getAmountDrinksOrderedForCurrentTable(item) : model.getAmountFoodsOrderedForCurrentTable(item))
                
                if self.sliderRemove.maximumValue == 0 {
                    self.removeMany.setTitle("No item to remove", forState: UIControlState.Normal)
                }
            }
            
            if let amount:Int = amountOrdered {
                self.labelOrdered.text = "Currently \(amount) \(item.Description) ordered."
            }
        }
        
        if let c = color {
            self.view.backgroundColor = c
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func cancel(sender: UIBarButtonItem) {
        exit()
    }
    @IBAction func sliderChanged(sender: UISlider) {
        sender.setValue(Float(Int(sender.value + 0.5)), animated: true)
        
        if let item:OrderItem = self.orderItem {
            if sender == sliderOrder {
                self.orderMany.setTitle("Order \(Int(sender.value)) \(item.Description)", forState: UIControlState.Normal)
            } else {
                self.removeMany.setTitle("Remove \(Int(sender.value)) \(item.Description)", forState: UIControlState.Normal)
            }
        }
        
        if sender == sliderOrder {
            if let item:OrderItem = self.orderItem {
                self.priceOfMany.text = "Price of \(Int(self.sliderOrder.value)) \(item.Description): £\(item.Price * self.sliderOrder.value)"
            }
        }
    }
    @IBAction func OrderMany(sender: UIButton) {
        if let orderDrink:Bool = self.isDrink {
            if orderDrink {
                model.orderDrinkForCurrentTable(self.orderItem!, amount: Int(self.sliderOrder.value))
            } else {
                model.orderFoodForCurrentTable(self.orderItem!, amount: Int(self.sliderOrder.value))
            }
            exit()
        }
    }
    @IBAction func removeMany(sender: UIButton) {
        if let orderDrink:Bool = self.isDrink {
            if orderDrink {
                model.orderDrinkForCurrentTable(self.orderItem!, amount: -Int(self.sliderRemove.value))
            } else {
                model.orderFoodForCurrentTable(self.orderItem!, amount: -Int(self.sliderRemove.value))
            }
            exit()
        }
    }
    @IBAction func clearOrder(sender: UIButton) {
        if let orderDrink:Bool = self.isDrink {
            if orderDrink {
                model.orderDrinkForCurrentTable(self.orderItem!, amount: -model.getAmountDrinksOrderedForCurrentTable(self.orderItem!))
            } else {
                model.orderFoodForCurrentTable(self.orderItem!, amount: -model.getAmountFoodsOrderedForCurrentTable(self.orderItem!))
            }
            exit()
        }
    }
    
    func exit() {
        if let completion:() -> Void = self.completionHandler {
            completion()
        }
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}
