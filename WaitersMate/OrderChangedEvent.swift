import UIKit

// global model instance
let orderChangedEvent = OrderChangedEvent()

class OrderChangedEvent {
    
    // contains all functions that should be executed when the current order changes
    var functions = Array<() -> ()>()
    
    // order has changed, trigger event
    func onOrderChanged() {
        for function in functions {
            function()
        }
    }
    
    // add function to array
    func addOnOrderChangedClosure(function: () -> ()) {
        self.functions.append(function)
    }
}
