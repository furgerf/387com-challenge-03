//
//  SecondViewController.swift
//  WaitersMate
//
//  Created by Admin on 28/10/2014.
//  Copyright (c) 2014 Coventry University. All rights reserved.
//

import UIKit

class DrinksViewController : UITableViewController, UISearchBarDelegate, UIGestureRecognizerDelegate {
    
    // outlets
    @IBOutlet var drinksTable: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet var swipeLeft: UISwipeGestureRecognizer!
    @IBOutlet var swipeRight: UISwipeGestureRecognizer!
    
    @IBAction func swipeLeftGesture(sender: UISwipeGestureRecognizer) {
        // for some reason, xcode won't build when using an animation
        // displaying a nonsensical error message about missing parameters
        // so page transistions are boring
        self.tabBarController?.selectedIndex++
    }
    @IBAction func swipeRightGesture(sender: UISwipeGestureRecognizer) {
        // for some reason, xcode won't build when using an animation
        // displaying a nonsensical error message about missing parameters
        // so page transistions are boring
        self.tabBarController?.selectedIndex--
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        // hide keyboard like that rather than with swipe since swipe doesnt work with the underlaying tableview
        self.searchBar.resignFirstResponder()
    }
    
    // indices of currently visible drinks
    var drinkIndices = [Int]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // show all drinks
        for (drinkIndex, drink) in enumerate(model.drinks) {
            drinkIndices.append(drinkIndex)
        }
        
        // set search bar delegate
        self.searchBar.delegate = self
        
        // add data refresh to tablechangedevent
        tableChangedEvent.addOnTableChangedClosure({
            self.drinksTable.reloadData()
        })
        orderChangedEvent.addOnOrderChangedClosure({
            self.drinksTable.reloadData()
        })
        
        self.view.addGestureRecognizer(self.swipeLeft)
        self.view.addGestureRecognizer(self.swipeRight)
        self.view.backgroundColor = drinksColor
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the± section.
        
        return drinkIndices.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("DrinkItem", forIndexPath: indexPath) as UITableViewCell
        
        // Configure the cell...
        cell.textLabel.text = model.drinks[drinkIndices[indexPath.row]].Description
        cell.detailTextLabel?.text = "(\(model.tableDrinks[model.currentTableIndex][drinkIndices[indexPath.row]].description))"
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.setCellColor(tableView.cellForRowAtIndexPath(indexPath)!, color: drinksColorHighlighted)
    }
    
    override func tableView(tableView: UITableView, didHighlightRowAtIndexPath indexPath: NSIndexPath) {
        self.setCellColor(tableView.cellForRowAtIndexPath(indexPath)!, color: drinksColorHighlighted)
    }
    
    override func tableView(tableView: UITableView, didUnhighlightRowAtIndexPath indexPath: NSIndexPath) {
        self.setCellColor(tableView.cellForRowAtIndexPath(indexPath)!, color: drinksColor)
    }
    
    func setCellColor(cell: UITableViewCell, color: UIColor){
        cell.contentView.backgroundColor = color
        cell.backgroundColor = color
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        // remove all previous indices
        drinkIndices.removeAll(keepCapacity: false)
        
        // fill with new indices
        for (drinkIndex, drink) in enumerate(model.drinks) {
            if searchBar.text == "" || drink.Description.lowercaseString.rangeOfString(searchBar.text.lowercaseString) != nil {
                drinkIndices.append(drinkIndex)
            }
        }
        
        // refresh entries
        self.drinksTable.reloadData()
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView!, canEditRowAtIndexPath indexPath: NSIndexPath!) -> Bool {
    // Return NO if you do not want the specified item to be editable.
    return true
    }
    */
    
    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView!, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath!) {
    if editingStyle == .Delete {
    // Delete the row from the data source
    tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
    } else if editingStyle == .Insert {
    // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
    }
    */
    
    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView!, moveRowAtIndexPath fromIndexPath: NSIndexPath!, toIndexPath: NSIndexPath!) {
    
    }
    */
    
    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView!, canMoveRowAtIndexPath indexPath: NSIndexPath!) -> Bool {
    // Return NO if you do not want the item to be re-orderable.
    return true
    }
    */
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
        if segue.identifier == "ShowOrderItemDetails" {
            let destination = segue.destinationViewController as OrderItemDetailViewController
            let indexPath = self.drinksTable.indexPathForSelectedRow()
            if let row:Int = indexPath?.row {
                destination.orderItem =  model.drinks[drinkIndices[row]]
                destination.amountOrdered = model.tableDrinks[model.currentTableIndex][drinkIndices[row]]
                destination.isDrink = true
                destination.completionHandler = { self.drinksTable.reloadData(); }
                destination.color = drinksColor
            }
        }
    }
}

