//
//  TabController.swift
//  WaitersMate
//
//  Created by admin on 11/24/14.
//  Copyright (c) 2014 Coventry University. All rights reserved.
//

import UIKit

class TabController: UITabBarController {
    @IBOutlet var swipeLeft: UISwipeGestureRecognizer!
    @IBAction func swipeLeftAction(sender: UISwipeGestureRecognizer) {
        UIView.animateWithDuration(0.5, animations: {
            self.tabBa
            }, completion: {
                (value: Bool) in
                self.tabBarController?.selectedIndex++
                UIView.animateWithDuration(0.5, animations: {
                })
        })
    }

    @IBOutlet var swipeRight: UISwipeGestureRecognizer!
    @IBAction func swipeRightAction(sender: UISwipeGestureRecognizer) {
        UIView.animateWithDuration(0.5, animations: {
            self.tableTable.alpha = 0
            }, completion: {
                (value: Bool) in
                self.tabBarController?.selectedIndex++
                UIView.animateWithDuration(0.5, animations: {
                })
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
