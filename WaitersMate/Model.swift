import UIKit

// global model instance
let model = Model()

class Model {
    // numbers for all available tables
    let tables:[Int] = [ 1, 2, 3, 4, 5, 6 ]
    
    // all available drinks
    let drinks:[OrderItem] = [
        OrderItem(Description: "Water (0.5l)", Price: 0.5),
        OrderItem(Description: "Water (1l)", Price: 0.75),
        OrderItem(Description: "Black Tea", Price: 1.5),
        OrderItem(Description: "Green Tea", Price: 1.5),
        OrderItem(Description: "Coca Cola", Price: 1.75),
        OrderItem(Description: "Fanta", Price: 1.75),
        OrderItem(Description: "Sprite", Price: 1.75),
        OrderItem(Description: "Ice Tea", Price: 1.75),
        OrderItem(Description: "Beer", Price: 3),
        OrderItem(Description: "Red wine", Price: 12),
        OrderItem(Description: "White wine", Price: 11),
        OrderItem(Description: "Coffee", Price: 2.75),
        OrderItem(Description: "Espresso", Price: 2.5),
        OrderItem(Description: "Cappuchino", Price: 3)
    ]
    
    // contains counts of ordered drinks per table
    var tableDrinks:[[Int]] = Array<Array<Int>>()
    
    // all available foods
    let foods:[OrderItem] = [
        OrderItem(Description: "Small salad", Price: 2),
        OrderItem(Description: "Green salad", Price: 2.5),
        OrderItem(Description: "Mixed salad", Price: 2.5),
        OrderItem(Description: "Tomato soup", Price: 2.25),
        OrderItem(Description: "Vegetable soup", Price: 2.75),
        OrderItem(Description: "Spaghetti", Price: 6),
        OrderItem(Description: "Lasagne", Price: 7.5),
        OrderItem(Description: "Risotto", Price: 6.5),
        OrderItem(Description: "Steak", Price: 10.25),
        OrderItem(Description: "Chinese noodles", Price: 8),
        OrderItem(Description: "Pizza", Price: 4.5),
        OrderItem(Description: "Fish and Chips", Price: 4.5),
        OrderItem(Description: "Hotdog", Price: 2.5),
        OrderItem(Description: "Kebap", Price: 5)
    ]
    
    // contains counts of ordered foods per table
    var tableFoods:[[Int]] = Array<Array<Int>>()
    
    // index of table that is currently being served
    var currentTableIndex:Int = 0
    
    // data persistence
    let DATA_DRINKS = "drinks"
    let DATA_FOODS = "foods"
    let savedData:NSUserDefaults = NSUserDefaults.standardUserDefaults()
    
    // returns all drinks that have been ordered with the amount ordered
    func getDrinkOrderForCurrentTable() -> [(OrderItem, Int)]
    {
        var result = [(OrderItem, Int)]()
        for (drinkIndex, count) in enumerate(self.tableDrinks[self.currentTableIndex]) {
            if (count != 0) {
                result.append((self.drinks[drinkIndex], count))
            }
        }
        return result
    }
    
    // returns all foods that have been ordered with the amount ordered
    func getFoodOrderForCurrentTable() -> [(OrderItem, Int)]
    {
        var result = [(OrderItem, Int)]()
        for (foodIndex, count) in enumerate(self.tableFoods[self.currentTableIndex]) {
            if (count != 0) {
                result.append((self.foods[foodIndex], count))
            }
        }
        return result
    }
    
    func getCostOfOrderForCurrentTable() -> Float {
        var cost:Float = 0.0
        var order = self.getDrinkOrderForCurrentTable() + self.getFoodOrderForCurrentTable()
        
        for (var i = 0; i < order.count; i++) {
            cost += order[i].0.Price * Float(order[i].1 as Int)
        }
        
        return cost
    }
    
    func getAmountDrinksOrderedForCurrentTable(item:OrderItem) -> Int {
        return  self.tableDrinks[self.currentTableIndex][find(self.drinks, item)!]
    }
    
    func getAmountFoodsOrderedForCurrentTable(item:OrderItem) -> Int {
        return self.tableFoods[self.currentTableIndex][find(self.foods, item)!]
    }
    
    // order one drink with index
    func orderDrinkForCurrentTable(drink:OrderItem, amount:Int){
        self.tableDrinks[self.currentTableIndex][find(self.drinks, drink)!] += amount
        self.savedData.setObject(self.tableDrinks, forKey: self.DATA_DRINKS)
        self.savedData.synchronize()
        
        orderChangedEvent.onOrderChanged()
    }
    
    // order one food with index
    func orderFoodForCurrentTable(food:OrderItem, amount:Int){
        self.tableFoods[self.currentTableIndex][find(self.foods, food)!] += amount
        self.savedData.setObject(self.tableFoods, forKey: self.DATA_FOODS)
        self.savedData.synchronize()
        
        orderChangedEvent.onOrderChanged()
    }
    
    // returns true if current table has ordered at least one drink
    func hasCurrentTableOrderedDrinks() -> Bool{
        return self.hasTableOrderedDrinks(self.currentTableIndex)
    }
    // returns true if table has ordered at least one drink
    func hasTableOrderedDrinks(tableIndex: Int) -> Bool
    {
        for i in 0...self.tableDrinks[tableIndex].count - 1 {
            if self.tableDrinks[tableIndex][i] > 0 {
                return true
            }
        }
        return false
    }
    
    // returns true if current table has ordered at least one drink
    func hasCurrentTableOrderedFoods() -> Bool{
        return self.hasTableOrderedFoods(self.currentTableIndex)
    }
    // returns true if table has ordered at least one food
    func hasTableOrderedFoods(tableIndex: Int) -> Bool
    {
        for i in 0...tableFoods[tableIndex].count - 1 {
            if self.tableFoods[tableIndex][i] > 0 {
                return true
            }
        }
        return false
    }
    
    func clearCurrentTable() {
        for i in 0...self.tableDrinks[self.currentTableIndex].count - 1 {
            self.tableDrinks[self.currentTableIndex][i] = 0
        }
        for i in 0...self.tableFoods[self.currentTableIndex].count - 1 {
            self.tableFoods[self.currentTableIndex][i] = 0
        }
        
        self.savedData.setObject(self.tableDrinks, forKey: self.DATA_DRINKS)
        self.savedData.setObject(self.tableFoods, forKey: self.DATA_FOODS)
        self.savedData.synchronize()
        
        orderChangedEvent.onOrderChanged()
    }
    
    init()
    {
        // loading data
        if (self.savedData.objectForKey(self.DATA_DRINKS) != nil) {
            self.tableDrinks = self.savedData.arrayForKey(self.DATA_DRINKS) as [[Int]]
        } else {
            for tableIndex in 0...self.tables.count - 1 {
                self.tableDrinks.append(Array(count:self.drinks.count, repeatedValue:0))
            }
        }
        if (self.savedData.objectForKey(self.DATA_FOODS) != nil) {
            self.tableFoods = self.savedData.arrayForKey(self.DATA_FOODS) as [[Int]]
        } else {
            for tableIndex in 0...self.tables.count - 1 {
                self.tableFoods.append(Array(count:self.foods.count, repeatedValue:0))
            }
        }
    }
}
