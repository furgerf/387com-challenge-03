import UIKit

// create struct for orderitem where instances can be compared
struct OrderItem : Equatable {
    var Description:String
    var Price:Float
}

// unsurprisingly, instances are equal if their properties match
func ==(lhs: OrderItem, rhs: OrderItem) -> Bool {
    return lhs.Description == rhs.Description && lhs.Price == rhs.Price
}

