//
//  SecondViewController.swift
//  WaitersMate
//
//  Created by Admin on 28/10/2014.
//  Copyright (c) 2014 Coventry University. All rights reserved.
//

import UIKit

class OrderViewController : UITableViewController, UIGestureRecognizerDelegate {
   
    // outlets
    @IBOutlet var ordersTable: UITableView!
    
    @IBOutlet var swipeRight: UISwipeGestureRecognizer!
    @IBAction func swipeRightGesture(sender: UISwipeGestureRecognizer) {
        // for some reason, xcode won't build when using an animation
        // displaying a nonsensical error message about missing parameters
        // so page transistions are boring
        self.tabBarController?.selectedIndex--
    }
    
    // section data
    var sections = [ "Drinks", "Food", "Order" ]
   
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // add data refresh to tablechangedevent
        tableChangedEvent.addOnTableChangedClosure({
            self.ordersTable.reloadData()
        })
        orderChangedEvent.addOnOrderChangedClosure({
            self.ordersTable.reloadData()
        })
        
        self.view.addGestureRecognizer(swipeRight)
        self.view.backgroundColor = orderColor
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        
        return 3
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the± section.
        
        // number of rows depends on section (drinks or foods)
        if section == 0 {
            return model.getDrinkOrderForCurrentTable().count == 0 ? 1 : model.getDrinkOrderForCurrentTable().count
        }
        if section == 1 {
            return model.getFoodOrderForCurrentTable().count == 0 ? 1 : model.getFoodOrderForCurrentTable().count
        }
        if section == 2 {
            return 2
        }
        
        return -1
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("OrderItem", forIndexPath: indexPath) as UITableViewCell
        
        // Configure the cell...
        switch indexPath.section {
        case 0:
            if model.getDrinkOrderForCurrentTable().count == 0 {
                cell.textLabel.text = "(no drinks ordered)"
                cell.detailTextLabel?.text = ""
                cell.textLabel.textColor = UIColor(red: 0.4, green: 0.4, blue: 0.4, alpha: 1)
            } else {
                cell.textLabel.text = model.getDrinkOrderForCurrentTable()[indexPath.row].0.Description
                cell.detailTextLabel?.text = "(\(model.getDrinkOrderForCurrentTable()[indexPath.row].1))"
                cell.textLabel.textColor = UIColor.blackColor()
            }
            break
        case 1:
            if model.getFoodOrderForCurrentTable().count == 0 {
                cell.textLabel.text = "(no food ordered)"
                cell.detailTextLabel?.text = ""
                cell.textLabel.textColor = UIColor(red: 0.4, green: 0.4, blue: 0.4, alpha: 1)
            } else {
                cell.textLabel.text = model.getFoodOrderForCurrentTable()[indexPath.row].0.Description
                cell.detailTextLabel?.text = "(\(model.getFoodOrderForCurrentTable()[indexPath.row].1))"
                cell.textLabel.textColor = UIColor.blackColor()
            }
            break
        case 2:
            cell.textLabel.textColor = UIColor.redColor()
            cell.detailTextLabel?.text = ""
            if indexPath.row == 0 {
                cell.textLabel.text = "Total cost of order: £\(model.getCostOfOrderForCurrentTable())"
            } else {
                cell.textLabel.text = "Clear order"
            }
        default:
            println("ERROR, unexpected indexpath section")
        }
        
        return cell
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.sections[section]
    }
    
    var selectedIndex:NSIndexPath?

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.setCellColor(tableView.cellForRowAtIndexPath(indexPath)!, color: orderColorHighlighted)
        
        if indexPath.section > 1 {
            if indexPath.row == 1 {
                var alert = UIAlertController(title: "Confirm", message: "Are you sure you want to clear the order?", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default, handler: {
                    (action) -> Void in
                        model.clearCurrentTable()
                        self.tabBarController?.selectedIndex = 0
                }))
                alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
            }
            
            return
        }
        
        if indexPath.section == 0 && !model.hasCurrentTableOrderedDrinks() {
            return
        }
        
        if indexPath.section == 1 && !model.hasCurrentTableOrderedFoods() {
            return
        }
        
        self.selectedIndex = indexPath
        
        self.performSegueWithIdentifier("ShowOrderItemDetails", sender: self)
    }
    
    override func tableView(tableView: UITableView, didHighlightRowAtIndexPath indexPath: NSIndexPath) {
        self.setCellColor(tableView.cellForRowAtIndexPath(indexPath)!, color: orderColorHighlighted)
    }
    
    override func tableView(tableView: UITableView, didUnhighlightRowAtIndexPath indexPath: NSIndexPath) {
        self.setCellColor(tableView.cellForRowAtIndexPath(indexPath)!, color: orderColor)
    }
    
    func setCellColor(cell: UITableViewCell, color: UIColor){
        cell.contentView.backgroundColor = color
        cell.backgroundColor = color
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView!, canEditRowAtIndexPath indexPath: NSIndexPath!) -> Bool {
    // Return NO if you do not want the specified item to be editable.
    return true
    }
    */
    
    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView!, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath!) {
    if editingStyle == .Delete {
    // Delete the row from the data source
    tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
    } else if editingStyle == .Insert {
    // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
    }
    */
    
    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView!, moveRowAtIndexPath fromIndexPath: NSIndexPath!, toIndexPath: NSIndexPath!) {
    
    }
    */
    
    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView!, canMoveRowAtIndexPath indexPath: NSIndexPath!) -> Bool {
    // Return NO if you do not want the item to be re-orderable.
    return true
    }
    */
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
        if segue.identifier == "ShowOrderItemDetails" {
        
            let destination = segue.destinationViewController as OrderItemDetailViewController
            
            if let index = self.selectedIndex {
                destination.orderItem = index.section == 0 ? model.getDrinkOrderForCurrentTable()[index.row].0 : model.getFoodOrderForCurrentTable()[index.row].0
                destination.amountOrdered = index.section == 0 ? model.getDrinkOrderForCurrentTable()[index.row].1 : model.getFoodOrderForCurrentTable()[index.row].1
                destination.isDrink = index.section == 0
                destination.completionHandler = { self.ordersTable.reloadData(); }
                destination.color = orderColor
            }else {
                println("ERROR: selectedIndex NOT SET!!")
            }
            
            self.selectedIndex = nil
        }
    }
    
    override func shouldPerformSegueWithIdentifier(identifier: String?, sender: AnyObject?) -> Bool {
        return self.selectedIndex != nil
    }
}

