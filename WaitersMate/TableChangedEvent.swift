import UIKit

// global model instance
let tableChangedEvent = TableChangedEvent()

class TableChangedEvent {
    
    // contains all functions that should be executed when the currently selected table changes
    var functions = Array<() -> ()>()
    
    // table has changed, trigger event
    func onTableChanged() {
        for function in functions {
            function()
        }
    }
    
    // add function to array
    func addOnTableChangedClosure(function: () -> ()) {
        self.functions.append(function)
    }
}
